rootProject.name = "gradle-npm-repository-plugin"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
    }
}