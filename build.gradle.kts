group = "com.artfable.gradle"
version = "0.1.0"

plugins {
    kotlin("jvm") version "1.8.20"
    `kotlin-dsl`
    `maven-publish`
    id("artfable.artifact") version "0.0.4"
}

val gitlabToken = findProperty("gitlabPersonalApiToken") as String?

repositories {
    mavenLocal()
    mavenCentral()
}

val jakson_version = "2.15.2"
val junit_version = "5.9.3"
val mockito_version = "5.4.0"

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("com.fasterxml.jackson.core:jackson-core:$jakson_version")
    implementation("com.fasterxml.jackson.core:jackson-databind:$jakson_version")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junit_version")
    testImplementation("org.mockito:mockito-core:$mockito_version")
    testImplementation("org.mockito:mockito-junit-jupiter:$mockito_version")
}

gradlePlugin {
    plugins {
        create("npmRepositoryPlugin") {
            id = "artfable.npm"
            implementationClass = "com.artfable.gradle.npm.repository.GradleNpmRepositoryPlugin"
        }
    }
}

tasks {
    test {
        useJUnitPlatform()
    }
    compileKotlin {
        kotlinOptions.jvmTarget = "17"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "17"
    }
    compileJava {
        sourceCompatibility = "17"
    }
    compileTestJava {
        targetCompatibility = "17"
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            artifact(tasks.sourceJar)
            artifact(tasks.javadocJar)

            pom {
                description.set("The plugin that allowed load frontend dependencies from package.json")
                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://gitlab.com/artfable-public/gradle-plugins/gradle-npm-repository-plugin/-/raw/master/LICENSE")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("artfable")
                        name.set("Artem Veselov")
                        email.set("art-fable@mail.ru")
                    }
                }
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/47326583/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}
